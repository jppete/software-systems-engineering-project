/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package software.systems.project;

import java.time.LocalDate;
import java.util.Scanner;

/**
 *
 * @author Joe
 */

public class TimeOffRequest {
    
    public enum ApprovalStatus {APPROVED, DENIED, PENDING}
    
    private LocalDate _startDate;
    private LocalDate _endDate;
    private ApprovalStatus _approval;
    private int _employee;
    
    //Constructors
    public TimeOffRequest()
    {
         
    }
    
    public TimeOffRequest(LocalDate startdate, LocalDate endDate, ApprovalStatus approval, int employee)
    {
       _startDate = startdate;
       _endDate = endDate;
       _approval = approval;
       _employee = employee; 
    }

    /**
     * @return the _startDate
     */
    public LocalDate getStartDate() {
        return _startDate;
    }

    /**
     * @param _startDate the _startDate to set
     */
    public void setStartDate(LocalDate _startDate) {
        this._startDate = _startDate;
    }

    /**
     * @return the _endDate
     */
    public LocalDate getEndDate() {
        return _endDate;
    }

    /**
     * @return the _employee
     */
    public int getEmployee() {
        return _employee;
    }

    /**
     * @param _employee the _employee to set
     */
    public void setEmployee(int _employee) {
        this._employee = _employee;
    }

    /**
     * @param _endDate the _endDate to set
     */
    public void setEndDate(LocalDate _endDate) {
        this._endDate = _endDate;
    }

    /**
     * @return the _approval
     */
    public ApprovalStatus getApproval() {
        return _approval;
    }

    /**
     * @param _approval the _approval to set
     */
    public void setApproval(ApprovalStatus _approval) {
        this._approval = _approval;
    }
    
    //print the time off request
    public void printRequest()
    {
        System.out.println("Employee Number: "+this.getEmployee()+"\n");
        System.out.println("From: "+this.getStartDate().toString()+" To: "+this.getEndDate().toString()+"\n");
        
        switch (this._approval)
        {
            case APPROVED:
                System.out.println("APPROVED \n");
                break;
            case DENIED:
                System.out.println("DENIED \n");
                break;
            case PENDING:
                System.out.println("PENDING \n");
                break;
        }
    }
    
     //creates a new time off request for the employee in the calendar year
    public static TimeOffRequest createRequest(int employeeNumber, int year)
    {
        Scanner scan = new Scanner(System.in);
        System.out.println("Please enter the start date of the request for year "+year+"\n");
        System.out.println("It needs to be in the form mm/dd");
        String start = scan.next();
        String startSplit[] = start.split("/");
        LocalDate startDate = LocalDate.of(year, Integer.parseInt(startSplit[0]), Integer.parseInt(startSplit[1]));
        
        System.out.println("Please enter the end date of the request for year "+year+"\n");
        System.out.println("It needs to be in the form mm/dd");
        String end = scan.next();
        String endSplit[] = end.split("/");
        LocalDate endDate = LocalDate.of(year, Integer.parseInt(endSplit[0]), Integer.parseInt(endSplit[1]));
        
        return new TimeOffRequest(startDate, endDate, TimeOffRequest.ApprovalStatus.PENDING, employeeNumber);
    }
    
   
}
