/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package software.systems.project;

import Calendar.TimeOff.TimeOffCalendar;
import java.util.Scanner;
/**
 *
 * @author Joe
 */
//This is the main class That runs the system
public class TimeOffSystem {
    
    private TimeOffCalendar _systemCalendar;
    private BuinessRules _systemRules;
    private ManagerApproval _systemManager;
    
    //start the system!
    TimeOffSystem()
    {
        boolean inputValid = false;
        
        Scanner scan = new Scanner (System.in);
        System.out.println("Intialzing System \n");
        System.out.println("What year is it? Please enter YYYY \n");
        int year = scan.nextInt();
        _systemCalendar = new TimeOffCalendar(year);

        boolean boolApprove = false;
        while (!inputValid)
        {
            System.out.println("Do employees need managers approval? \n");
            System.out.println("Y or N \n");
            String approve = scan.next();
            if (approve.equals("Y"))
            {
                boolApprove = true;
                _systemManager = new ManagerApproval();
                inputValid = true;
            }
            else if(approve.equals("N"))
            {
                boolApprove = false;
                _systemManager = null;
                inputValid = true;
            }
        }
        
        System.out.println("How many People are allowed Off per day? \n");
        int numPeople = scan.nextInt();
        
        _systemRules = new BuinessRules(numPeople, boolApprove);
    }
    
    //getters
    public TimeOffCalendar getSystemCalendar()
    {
        return _systemCalendar;
    }
    public BuinessRules getSystemRules()
    {
        return _systemRules;
    }
    public ManagerApproval getSystemManager()
    {
        return _systemManager;
    }
    
    public int getEmployeeNumber()
    {
        Scanner scan = new Scanner (System.in);
        System.out.println("Please enter your employee number \n");
        int eNumber = scan.nextInt();
        return eNumber;
    }
    
    //main method everything Starts from here!
    public static void main(String args[])
    {
        TimeOffSystem runningSystem = new TimeOffSystem();
        
        Scanner scan = new Scanner(System.in);
        boolean quit = false;
        
        while(!quit)
        {
            System.out.println("Are you a Manager or Employee? or Quit? \n" );
            System.out.println("Enter M or E or Q \n");
            
            String input = scan.next();
            switch (input)
            {
                case "M": 
                        if (runningSystem.getSystemManager() == null)
                        {
                            System.out.println("Sorry this system was setup to not need manager approval \n");
                        }
                        else
                        {
                            runningSystem.getSystemManager().managerReview(runningSystem.getSystemCalendar());
                        }
                        break;
                case "E":
                    int eNumber = runningSystem.getEmployeeNumber();
                    
                    TimeOffRequest newRequest = null;
                    
                    //need to check if you need a managers approval
                    //this way we can ask if the employee wants to check on previous requests
                    if (runningSystem.getSystemManager() != null)
                        {
                            boolean inputValid = false;
                            String input2 = "";
                            
                            while(inputValid == false)
                            {
                                System.out.println("Would You Like to create a New request or check for request Approval? \n");
                                System.out.println("Enter N or A \n");
                                input2 = scan.next();
                                //if employee wants to enter a new request
                                if (input2.equals ("N"))
                                {
                                    System.out.println("Create a New Request! \n");
                                    newRequest = TimeOffRequest.createRequest(eNumber, runningSystem.getSystemCalendar().getCalendarYear());
                                    runningSystem.getSystemRules().allowTimeOff(newRequest, runningSystem.getSystemCalendar());
                                    //if the request is pending manager approval add to the request approval list
                                    if(newRequest.getApproval() == TimeOffRequest.ApprovalStatus.PENDING)
                                    {
                                        runningSystem.getSystemManager().addRequestForApproval(newRequest);
                                    }
                                    newRequest.printRequest();
                                    inputValid = true;
                                }
                                else if (input2.equals("A"))
                                {
                                    runningSystem.getSystemManager().employeeReview(eNumber);
                                    inputValid = true;
                                }
                            }
                        }
                        //if you do not need managers approval
                        else
                        {
                            System.out.println("Create a New Request! \n");
                            //get employees request
                            newRequest = TimeOffRequest.createRequest(eNumber, runningSystem.getSystemCalendar().getCalendarYear()); 
                            runningSystem.getSystemRules().allowTimeOff(newRequest, runningSystem.getSystemCalendar());
                            //if it is approved put in the calendar
                            if (newRequest.getApproval() == TimeOffRequest.ApprovalStatus.APPROVED)
                            {
                                runningSystem.getSystemCalendar().enterTimeOffRequestInCalendar(newRequest);
                            }
                            newRequest.printRequest();
                        }                    
                    break;
                case "Q":
                    quit = true;
                    break;   
            } 
        }
    }
   
}
