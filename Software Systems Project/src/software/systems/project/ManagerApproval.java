/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package software.systems.project;

import java.util.ArrayList;
import java.util.Scanner;
import Calendar.TimeOff.TimeOffCalendar;

/**
 *
 * @author Jill
 */
public class ManagerApproval {
 
    //holds requests waiting for managers approval
    private ArrayList<TimeOffRequest> _needManApproval;
    //holds requests that have been declined
    private ArrayList<TimeOffRequest> _manReviewedRequests;

    public ManagerApproval()
    {
        _needManApproval = new ArrayList<TimeOffRequest>();
        _manReviewedRequests = new ArrayList<TimeOffRequest>();
    }
    
    //getters and setters
    public int getNumberOfRequestNeedApproval()
    {
        return _needManApproval.size();
    }

    public void addRequestForApproval(TimeOffRequest request) 
    {
        _needManApproval.add(request);
    }

    public TimeOffRequest returnRequestForApproval (int index)
    {
        return _needManApproval.get(index);
    }

    public void addReviewedRequest(TimeOffRequest request)
    {
        _manReviewedRequests.add(request);
    }

    public TimeOffRequest returnReviewedRequest(int index)
    {
        return _manReviewedRequests.get(index);
    }

    public void removeRequestManagerList(TimeOffRequest request)
    {
        _needManApproval.remove(request);
    }

    public void removeRequestFromReviewedList (TimeOffRequest request)
    {
        _manReviewedRequests.remove(request);
    }
    
    //used to print out approvals after manager review
    public void employeeReview(int employeeNumber)
    {
        if (_manReviewedRequests.isEmpty())
        {
            System.out.println("There is Currenly no Time Off Approvals from the Manager. \n");
        }
        else
        {
            ArrayList<TimeOffRequest> printedRequests = new ArrayList();
            
            for(TimeOffRequest i : _manReviewedRequests)
            {
                if (i.getEmployee() == employeeNumber)
                {
                    i.printRequest();
                    printedRequests.add(i);
                }
            }
            
            for(TimeOffRequest i : printedRequests)
            {
                this.removeRequestFromReviewedList(i);
            }
        }
    }

    //run for manager review - this method allow managers to approve or decline requests
    public void managerReview(TimeOffCalendar calendar)
    {
        Scanner scan = new Scanner(System.in);
        if (_needManApproval.isEmpty())
        {
            System.out.println("There Is no request that need approval at this time \n");
        }
        else
        {
            for(TimeOffRequest i : _needManApproval)
            {
                i.printRequest();

                boolean inputValid = false;
                String input = "";

                while(inputValid == false)
                {
                    System.out.println("Approve or Decline? Enter A or D \n");

                    input = scan.next();
                    if(input.equals("A"))
                    {
                        i.setApproval(TimeOffRequest.ApprovalStatus.APPROVED);
                        calendar.enterTimeOffRequestInCalendar(i);
                        this.addReviewedRequest(i);
                        inputValid = true;
                    }
                    else if(input.equals("D"))
                    {
                        i.setApproval(TimeOffRequest.ApprovalStatus.DENIED);
                        this.addReviewedRequest(i);
                        inputValid = false;
                    }
                }    
            }
        }

        _needManApproval.clear();
    }

}
