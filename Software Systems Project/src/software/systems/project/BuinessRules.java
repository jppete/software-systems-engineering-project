/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package software.systems.project;
import Calendar.TimeOff.TimeOffCalendar;

/**
 *
 * @author Joe
 */

//This class is so that the time off request can be approved or not
public class BuinessRules {
    
    //Used to determine the number of people allowed off per day
    private int _numOfPeopleAllowedOff;
    //used to set if request need manager approval
    private boolean _needManagerApproval;
    
    public BuinessRules(int numOfPeopleAllowedOff, boolean needManagersApproval)
    {
        _numOfPeopleAllowedOff = numOfPeopleAllowedOff;
        _needManagerApproval = needManagersApproval;
    }
    
    //input request and it will approve or deny the request
    public void allowTimeOff(TimeOffRequest request, TimeOffCalendar calendar)
    {
        //Check the dates
        boolean datesOK = calendar.checkDates(request.getStartDate(), request.getEndDate(), this.getNumOfPeopleAllowedOff());
        
        //check if dates and if they need approval
        if (datesOK == true && doesRequestNeedManagerApproval() == true)
        {
            request.setApproval(TimeOffRequest.ApprovalStatus.PENDING);
        }
        else if (datesOK == true && doesRequestNeedManagerApproval() == false)
        {
            request.setApproval(TimeOffRequest.ApprovalStatus.APPROVED);
        }
        else 
        {
            request.setApproval(TimeOffRequest.ApprovalStatus.DENIED);
        }
    }
    
    /**
     * @return the _numOfPeopleAllowedOff
     */
    public int getNumOfPeopleAllowedOff() {
        return _numOfPeopleAllowedOff;
    }

    /**
     * @param _numOfPeopleAllowedOff the _numOfPeopleAllowedOff to set
     */
    public void setNumOfPeopleAllowedOff(int _numOfPeopleAllowedOff) {
        this._numOfPeopleAllowedOff = _numOfPeopleAllowedOff;
    }

    /**
     * @return the _needManagerApproval
     */
    public boolean doesRequestNeedManagerApproval() {
        return _needManagerApproval;
    }

    /**
     * @param _needManagerApproval the _needManagerApproval to set
     */
    public void setNeedManagerApproval(boolean _needManagerApproval) {
        this._needManagerApproval = _needManagerApproval;
    }
    
    
    
}
