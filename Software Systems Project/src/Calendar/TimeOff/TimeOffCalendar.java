/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Calendar.TimeOff;

import java.time.LocalDate;
import software.systems.project.TimeOffRequest;

/**
 *
 * @author Joe
 */

//This holds the calendar
public class TimeOffCalendar {
    
    private String MONTHS[] =  {"JANUARY", "FEBUARY", "MARCH", "APRIL", "MAY", "JUNE", "JULY", "AUGUST", "SEPTEMBER", "OCTOBER", "NOVEMBER", "DECEMBER"};
    
    private int NUMDAYS[] = {31,28,31,30,31,30,31,31,30,31,30,31};
    
    private int _year;

    private Month _requestMonthList[];
    
    public TimeOffCalendar(int currentYear)
    {
        _requestMonthList = new Month[MONTHS.length];
        
        _year = currentYear;
        
        if (leapYear(currentYear))
        {
            NUMDAYS[1] = 29;
        }
        
        //add the months to the calendar with the correct amount of days
        for (int i = 0; i < MONTHS.length; i++)
        {
            _requestMonthList[i] = new Month(MONTHS[i], NUMDAYS[i]);
        }

    }
    
    //determine if it is a leap year
    public static boolean leapYear(int year)
    {
	//if it is a leap year return true
        if(((year % 400) == 0) || (((year % 4) == 0) && ((year % 100) != 0)))
	{
            return true; 
	}
	else 
	{
            return false;
        }
    }
    
    //return current int of year
    public int getCalendarYear()
    {
        return _year;
    }
    
    //put in the int of the month and it returns the month object for it
    public Month getMonth(int month)
    {
        int correctedMonth = month - 1;
        
        return _requestMonthList[correctedMonth];
    }
    
    //A bit tricky but I think I got it right
    //Need to make sure!!
    public boolean checkDates(LocalDate startDate, LocalDate endDate, int numOfPeopleAllowedOff)
    {
        boolean dateOk = true;
        
        int startMonth = startDate.getMonthValue();
        int startDay = startDate.getDayOfMonth();
        int endMonth = endDate.getMonthValue();
        int endDay = endDate.getDayOfMonth();
        
        //if the start date and end date are in the same month
        if (startMonth == endMonth)
        {
            for(int i = startDay; i <= endDay; i++)
            {
                if (this.getMonth(endMonth).getApprovalsForDay(i).numOffApprovedRequestOnDay() == numOfPeopleAllowedOff)
                {
                    dateOk = false;
                }
            }
        }
        //if the start date and end date are not in the same month
        else 
        {
            for(int i = startDay; i <= this.getMonth(startMonth).getNumberDaysInMonth(); i++)
            {
                if (this.getMonth(startMonth).getApprovalsForDay(i).numOffApprovedRequestOnDay() == numOfPeopleAllowedOff)
                {
                    dateOk = false;
                }
            }
            
            for(int i = 0; i <= endDay; i++)
            {
                if (this.getMonth(endDay).getApprovalsForDay(i).numOffApprovedRequestOnDay() == numOfPeopleAllowedOff)
                {
                    dateOk = false;
                }
            }
        }
        
        
        return dateOk;
    }
    
    //input the time off request into the calendar if it is approved
    public void enterTimeOffRequestInCalendar (TimeOffRequest request)
    {
        int startMonth = request.getStartDate().getMonthValue();
        int startDay = request.getStartDate().getDayOfMonth();
        int endMonth = request.getEndDate().getMonthValue();
        int endDay = request.getEndDate().getDayOfMonth();
        
        if (startMonth == endMonth)
        {
            for(int i = startDay; i <= endDay; i++)
            {
                this.getMonth(startMonth).getApprovalsForDay(i).addApprovedRequest(request);
            }
        }
        else 
        {
            for(int i = startDay; i <= this.getMonth(startMonth).getNumberDaysInMonth(); i++)
            {
                this.getMonth(startMonth).getApprovalsForDay(i).addApprovedRequest(request);
            }
            
            for(int i = 0; i <= endDay; i++)
            {
                this.getMonth(endMonth).getApprovalsForDay(i).addApprovedRequest(request);
            }
        }
    }
}
