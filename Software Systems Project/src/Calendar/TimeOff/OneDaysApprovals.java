/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Calendar.TimeOff;

import java.util.ArrayList;
import software.systems.project.TimeOffRequest;

/**
 *
 * @author Joe
 */

//this holds all the current approvals for the day
public class OneDaysApprovals {
    
    private ArrayList<TimeOffRequest> _daysApprovals;
    
    public OneDaysApprovals()
    {
        _daysApprovals = new ArrayList();
    }
    
    //add a new approved request
    public void addApprovedRequest(TimeOffRequest approvedRequest)
    {
        _daysApprovals.add(approvedRequest);
    }
    
    //get the current number of employees already approved for the day off
    public int numOffApprovedRequestOnDay()
    {
        if (_daysApprovals.isEmpty())
        {
            return 0;
        }
        else
        {
            return _daysApprovals.size();
        }
    }
    
    //get an approved request
    public TimeOffRequest getApprovedRequest(int index)
    {
        return _daysApprovals.get(index);
    }
    
}
