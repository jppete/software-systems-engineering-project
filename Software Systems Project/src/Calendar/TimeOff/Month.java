/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Calendar.TimeOff;

/**
 *
 * @author Joe
 */

//this holds all the days of the month
//each day holds that days current approvals
public class Month {
    
    private final String _name;
    private OneDaysApprovals _days[];
    
    public Month(String name, int numOfDay)
    {
        _name = name;
        _days = new OneDaysApprovals[numOfDay];
        
        for(int i = 0; i < numOfDay; i++)
        {
            _days[i] = new OneDaysApprovals();
        }
    }
    
    //get the OneDaysApprovals object so that you can add an approved request to calendar
    public OneDaysApprovals getApprovalsForDay(int day)
    {
        day -= 1;
        return _days[day];
    }
    
    //get the number of days in a month
    public int getNumberDaysInMonth()
    {
        return _days.length + 1;
    }
    
    //return the month name
    public String getMonthName ()
    {
        return _name;
    }
}
